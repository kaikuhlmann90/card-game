import React from 'react';
import { shallow } from 'enzyme';
import PlayerItem from 'components/PlayerItem';

const player: Player = {id: '0', realName: 'a', playerName: 'a', asset: 'a'};

describe('PlayerItem', () => {
  it('matches the snapshot', () => {
    const wrapper = shallow(<PlayerItem player={player} />);
    expect(wrapper).toMatchSnapshot();
  });
});
