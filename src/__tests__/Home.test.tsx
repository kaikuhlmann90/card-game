import React from 'react';
import { shallow } from 'enzyme';
import Home from 'containers/Home';

const player1 = {realName: 'a', playerName: 'a', asset: 'a'};
const player2 = {realName: 'b', playerName: 'b', asset: 'b'};
const player3 = {realName: 'c', playerName: 'c', asset: 'c'};
const players = [player2, player1, player3];

describe('Home', () => {
  const wrapper = shallow(<Home players={players} />);

  /**
   * Checks if sorting works
   * @param {string} id - The elements id
   * @param {string} expectedRealName - The expected realName after sorting
   */
  const sortsCorrectly = (id: string, expectedRealName: string) => {
    const sortAscButton = wrapper.find(id);
    sortAscButton.simulate('click');
    expect(players[0].realName).toEqual(expectedRealName);
  }

  it('matches the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('sorts players ascending', () => sortsCorrectly('#sortAscButton', 'a'));
  it('sorts players descending', () => sortsCorrectly('#sortDescButton', 'c'));
});
