import { isPlayerValid, isPlayerArrayValid, sortPlayers, Direction } from 'api/utils';

const player1 = {realName: 'a', playerName: 'a', asset: 'a'};
const player2 = {realName: 'b', playerName: 'b', asset: 'b'};
const player3 = {realName: 'c', playerName: 'c', asset: 'c'};
const noPlayer = {realName: 'a'};
const players = [player2, player1, player3];
const noPlayers = [...players, noPlayer];

describe('Util functions', () => {
  const sortsCorrectly = (direction: Direction, expectedRealName: string) => {
    const sortedPlayers = sortPlayers(players, direction, 'realName');
    expect(sortedPlayers[0].realName).toBe(expectedRealName);
  };
  
  it('detects if a player is valid', () => {
    expect(isPlayerValid(player1)).toBeTruthy();
  });

  it('detects if a player is invalid', () => {
    expect(isPlayerValid(noPlayer)).toBeFalsy();
  });

  it('detects if a player array is valid', () => {
    expect(isPlayerArrayValid(players)).toBeTruthy();
  });

  it('detects if a player array is invalid', () => {
    expect(isPlayerArrayValid(noPlayers)).toBeFalsy();
  });

  it('sorts players ascending', () => {
    sortsCorrectly('asc', 'a');
  });

  it('sorts players descending', () => {
    sortsCorrectly('desc', 'c');
  });
});
