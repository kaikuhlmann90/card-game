import React from 'react';
import { shallow } from 'enzyme';
import PlayerList from 'components/PlayerList';

const player: Player = {id: '0', realName: 'a', playerName: 'a', asset: 'a'};
const players: Player[] = [player, player];

describe('PlayerList', () => {
  it('matches the snapshot', () => {
    const wrapper = shallow(<PlayerList players={players} />);
    expect(wrapper).toMatchSnapshot();
  });
});
