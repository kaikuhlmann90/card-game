import React from 'react';
import { shallow } from 'enzyme';
import CardView from 'components/CardView';

describe('CardView', () => {
  it('matches the snapshot', () => {
    const wrapper = shallow(<CardView title='title'/>);
    expect(wrapper).toMatchSnapshot();
  });
});
