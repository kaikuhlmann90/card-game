export type Direction = 'asc' | 'desc';
export type SortBy = 'realName' | 'playerName' | 'asset';

/**
 * Error handler for network requests
 * @param {string} tag - The Class- or function name
 * @param {any} error - The thrown error
 */
export const handleError = (tag: string, error: any) => {
  console.error(`${tag}\n${error}`);
  throw error;
}

/**
 * Check if response data matches the Player type
 * @param {any} data - The response data to be validated
 * @returns {boolean}
 */
export const isPlayerValid = (data?: any): data is Player => {
  return data &&
         typeof data.realName === 'string' &&
         typeof data.playerName === 'string' &&
         typeof data.asset === 'string';
}

/**
 * Check if response data matches the Player type
 * @param {any} data - The response data to be validated
 * @returns {boolean}
 */
export const isPlayerArrayValid = (data?: any): data is Player[] => {
  return data &&
         Array.isArray(data) &&
         data.every(d => isPlayerValid(d));
}

/**
 * Sorts players by real name
 * @param {any} players - The players array to be sorted
 * @param {string} direction - The sort direction (asc or desc)
 * @param {string} sortBy - Either realName, playerName or asset
 * @returns {Player[]} players
 */
export const sortPlayers = (players: any, direction: Direction, sortBy: SortBy = 'realName'): Player[] => {
  if (!isPlayerArrayValid(players)) {
    return [];
  }

  const sortedPlayers: Player[] = players.sort((p1, p2) => {
    if (direction === 'asc') {
      return p2[sortBy] < p1[sortBy] ? 1 : -1;
    } else {
      return p2[sortBy] > p1[sortBy] ? 1 : -1;
    }
  });

  return sortedPlayers;
}
