import axios from 'axios';
import { handleError, isPlayerValid, isPlayerArrayValid } from 'api/utils';

// For testing purposes, you can modify and use:
// import playerData from 'res/players.json';

const PlayerApi = {

  /**
   * Get players from the network
   * @returns {Player[]} players
   */
  getPlayers: async () => {
    try {
      const url = 'https://bitbucket.org/!api/2.0/snippets/kaikuhlmann90/onRAML/a22b9d7cb87014f8523858afb68c6a38873f9aaf/files/card-game-data';
      const response = await axios.get(url);

      if (!isPlayerArrayValid(response.data)) {
        const errorData = JSON.stringify(response.data, null, 2);
        throw new Error(`Invalid response data: ${errorData}`);
      }
      
      return response.data as Player[];
    } catch (err) {
      handleError('getPlayers', err);
    }
  },

  /**
   * Submit a player to the network
   * @param {Player} player - The player to be submitted
   */
  submitPlayer: async (player: Player) => {
    try {
      if (!isPlayerValid(player)) {
        const errorData = JSON.stringify(player, null, 2);
        throw new Error(`Invalid player data: ${errorData}`);
      }

      await axios.post('', player);
    } catch (err) {
      handleError('submitPlayer', err);
    }
  }
}

export default PlayerApi;
