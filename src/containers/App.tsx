import React from 'react';
import { CssBaseline } from '@material-ui/core';
import { makeStyles, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Home from 'containers/Home';

const theme = createMuiTheme({
  palette: {
    type: 'dark'
  }
});

const App = React.memo(() => {
  const styles = useStyles();

  return (
    <div id='app' className={styles.app}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Home />
      </ThemeProvider>
    </div>
  );
});

const useStyles = makeStyles({
  app: {
    padding: 16
  }
});

export default App;
