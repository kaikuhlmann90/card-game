import React, { useState, useEffect } from 'react';
import { makeStyles, Container, Grid, Box, ButtonGroup, Button, Fade, Collapse, LinearProgress } from '@material-ui/core';
import { CardView, PlayerList, PlayerItem } from 'components';
import { sortPlayers, Direction } from 'api/utils';
import PlayerApi from 'api/PlayerApi';
import strings from 'res/strings';

interface Props {
  players?: Player[]
}

/**
 * Main container that displays the content of the game.
 * Configured to work on desktop computers with minimal responsive setup.
 */
const Home = React.memo((props: Props) => {
  const [ loading, setLoading ] = useState(true);
  const [ players, setPlayers ] = useState<Player[]>(props.players || []);
  const [ selectedPlayer, setSelectedPlayer ] = useState<Player>();
  const [ sortDirection, setSortDirection ] = useState('');
  const [ showDetails, setShowDetails ] = useState(false);
  const styles = useStyles();

  const onSortClick = (direction: Direction) => {
    const sortedPlayers = sortPlayers(players, direction, 'realName');
    setSortDirection(direction);
    setPlayers(sortedPlayers);
  };

  const onSubmitPlayerClick = async () => {
    try {
      await PlayerApi.submitPlayer(selectedPlayer as Player);
    } catch (err) {
      alert(strings.error.default);
    }
  };

  useEffect(() => {
    setLoading(true);

    const getPlayers = async () => {
      try {
        const updatedPlayer = await PlayerApi.getPlayers();
        setPlayers(updatedPlayer as Player[]);
        setLoading(false);
      } catch (err) {
        alert(strings.error.notFound);
      }
    }

    // Timout for testing purposes
    setTimeout(() => getPlayers(), 1000);
  }, []);

  useEffect(() => {
    if (selectedPlayer) {
      setTimeout(() => setShowDetails(true), 300);
    }
  }, [selectedPlayer]);

  const detailsContainer = (
    <Grid item xs={12} sm={4} className={`${styles.leftItem} ${selectedPlayer ? styles.animLeftItem : ''}`}>
      <Fade in={showDetails}>
        <Box className={styles.detailsCard}>
          {
            showDetails ?
            <CardView className={styles.detailsCard} title={strings.details}>
              <PlayerItem player={selectedPlayer} />
              <Button
                id='submitButton'
                fullWidth
                variant='contained'
                color='primary'
                className={styles.submitButton}
                onClick={() => onSubmitPlayerClick()}
              >
                {strings.submit}
              </Button>
            </CardView>
            : null
          }
        </Box>
      </Fade>
    </Grid>
  );

  const overviewContainer = (
    <Grid item xs={12} sm={8}>
      <CardView title={strings.overview}>
        {
          loading ?
          <LinearProgress color='primary' className={styles.progress} />
          : null
        }
        <Collapse in={!loading}>
          <Box>
            <ButtonGroup fullWidth className={styles.sortContainer}>
              <Button
                id='sortAscButton'
                className={`${styles.sortButton} ${sortDirection === 'asc' ? styles.sortButtonActive : ''}`}
                onClick={() => onSortClick('asc')}
              >
                {strings.sortAsc}
              </Button>
              <Button
                id='sortDescButton'
                className={`${styles.sortButton} ${sortDirection === 'desc' ? styles.sortButtonActive : ''}`}
                onClick={() => onSortClick('desc')}
              >
                {strings.sortDesc}
              </Button>
            </ButtonGroup>
            <PlayerList
              players={players}
              onPlayerSelected={setSelectedPlayer}
            />
          </Box>
        </Collapse>
      </CardView>
    </Grid>
  );

  return (
    <Container>
      <Grid container spacing={1} justify='center'>
        {detailsContainer}
        {overviewContainer}
      </Grid>
    </Container>
  );
});

const useStyles = makeStyles(theme => ({
  leftItem: {
    flex: 0,
    transition: theme.transitions.create('all', {
      duration: 300,
      easing: theme.transitions.easing.easeInOut
    })
  },

  animLeftItem: {
    flex: 1
  },

  detailsCard: {
    height: '100%'
  },

  submitButton: {
    marginTop: 'auto'
  },

  sortContainer: {
    marginBottom: 16
  },
  
  sortButton: {
    '&:hover, &:focus': {
      backgroundColor: theme.palette.secondary.light
    }
  },

  sortButtonActive: {
    backgroundColor: theme.palette.secondary.light
  },

  progress: {
    marginTop: 16
  }
}));

export default Home;
