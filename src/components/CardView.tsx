import React from 'react';
import { makeStyles, Card, CardHeader, CardContent } from '@material-ui/core';

interface Props {
  title: string;
  children?: React.ReactNode;
  className?: string;
  classNameContent?: string;
}

/**
 * Component used as a container for 'details' and 'overview'
 */
const CardView = React.memo((props: Props) => {
  const { title, children, className, classNameContent } = props;
  const styles = useStyles();

  return (
    <Card className={`${styles.card} ${className}`}>
      <CardHeader title={title} />
      <CardContent className={`${styles.cardContent} ${classNameContent}`}>
        {children}
      </CardContent>
    </Card>
  );
});

const useStyles = makeStyles(theme => ({
  card: {
    display: 'flex',
    flexDirection: 'column',
    padding: 8
  },

  cardContent: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  }
}));

export default CardView;
