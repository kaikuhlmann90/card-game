import React, { useState } from 'react';
import { makeStyles, Grid, Button } from '@material-ui/core';
import { PlayerItem } from 'components';

interface Props {
  players?: Player[];
  onPlayerSelected?: (player: Player) => void;
}

/**
 * Component used to display the player data in a grid-list
 */
const PlayerList = (props: Props) => {
  const { players, onPlayerSelected } = props;

  const [ selectedPlayer, setSelectedPlayer ] = useState<Player>();
  const styles = useStyles();
  
  const listItems = players ? players.map((player) => {
    const isSelected = selectedPlayer && player.id === selectedPlayer.id;
    const key = player.id || player.realName;

    return (
      <Grid item xs={4} key={key}>
        <Button
          variant='outlined'
          className={`${styles.item} ${isSelected ? styles.itemSelected : ''}`}
          onClick={() => {
            setSelectedPlayer(player);
            if (onPlayerSelected) onPlayerSelected(player);
          }}
        >
          <PlayerItem
            className={styles.itemTextContainer}
            player={player}
            ellipsisText
          />
        </Button>
      </Grid>
    );
  }) : null;

  return (
    <Grid container spacing={1}>
      {listItems}
    </Grid>
  );
}

const useStyles = makeStyles(theme => ({
  item: {
    width: '100%',
    textTransform: 'none',
    textAlign: 'left',
    padding: 0,
    '&:focus': {
      backgroundColor: theme.palette.primary.main
    }
  },

  itemSelected: {
    backgroundColor: theme.palette.primary.main
  },

  itemTextContainer: {
    padding: 8
  }
}));

export default PlayerList;
