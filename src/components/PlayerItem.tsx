import React from 'react';
import { Box, Typography, makeStyles } from '@material-ui/core';
import strings from 'res/strings';

interface Props {
  player?: Player,
  className?: string,
  ellipsisText?: boolean
}

/**
 * Component used to display the player data
 */
const PlayerItem = React.memo((props: Props) => {
  const { player, className, ellipsisText } = props;
  const styles = useStyles();

  const textStyle = ellipsisText ? styles.ellipsis : '';

  return (
    <Box className={`${styles.container} ${className}`}>
      <Box>
        <Typography component='p' className={`${styles.label} ${textStyle}`}>
          {`${strings.player.realName}:`}
        </Typography>
        <Typography id='realName' component='p' className={`${styles.text} ${textStyle}`}>
          {player ? player.realName : ''}
        </Typography>
      </Box>
      <Box>
        <Typography component='p' className={`${styles.label} ${textStyle}`}>
        {`${strings.player.playerName}:`}
        </Typography>
        <Typography id='playerName' component='p' className={`${styles.text} ${textStyle}`}>
          {player ? player.playerName : ''}
        </Typography>
      </Box>
      <Box>
        <Typography component='p' className={`${styles.label} ${textStyle}`}>
        {`${strings.player.asset}:`}
        </Typography>
        <Typography id='asset' component='p' className={`${styles.text} ${textStyle}`}>
          {player ? player.asset : ''}
        </Typography>
      </Box>
    </Box>
  );
});

const useStyles = makeStyles({
  container: {
    width: '100%',
    minHeight: 150,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },

  label: {
    fontSize: 10,
    fontWeight: 'bold',
    textTransform: 'uppercase'
  },

  text: {
    fontSize: 12
  },

  ellipsis: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden'
  }
});

export default PlayerItem;
