declare interface Player {
  id?: string;
  realName: string;
  playerName: string;
  asset: string;
}
