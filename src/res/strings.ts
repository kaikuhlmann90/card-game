import LocalizedStrings from 'react-localization';

const strings = new LocalizedStrings({
  en: {
    details: 'Details',
    overview: 'Overview',
    sortAsc: 'Sort asc',
    sortDesc: 'Sort desc',
    submit: 'Submit',

    player: {
      realName: 'Real name',
      playerName: 'Player name',
      asset: 'Asset'
    },

    error: {
      default: 'Something went wrong, try again please.',
      notFound: 'The requested resource could not be found.'
    }
  }
});

export default strings;
